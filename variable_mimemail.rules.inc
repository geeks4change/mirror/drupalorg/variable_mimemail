<?php

/**
 * @file
 * Rules actions for sending Mime-encoded emails.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function variable_mimemail_rules_action_info() {
  return array(
    'variable_mimemail_action' => array(
      'label' => t('Send MIME mail with Variable'),
      'group' => t('Variable Email'),
      'parameter' => array(
        'variable' => array(
          'type' => 'text',
          'label' => t('Variable'),
          'options list' => 'variable_email_variables_list',
          'description' => t('Select the variable which should be used as a template.'),
        ),
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t("The mail's recipient address. The formatting of this string must comply with RFC 2822."),
        ),
        'cc' => array(
          'type' => 'text',
          'label' => t('CC Recipient'),
          'description' => t("The mail's carbon copy address. You may separate multiple addresses with comma."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'bcc' => array(
          'type' => 'text',
          'label' => t('BCC Recipient'),
          'description' => t("The mail's blind carbon copy address. You may separate multiple addresses with comma."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'from_name' => array(
          'type' => 'text',
          'label' => t('Sender name'),
          'description' => t("The sender's name. Leave it empty to use the site-wide configured name."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'from_mail' => array(
          'type' => 'text',
          'label' => t('Sender e-mail address'),
          'description' => t("The sender's address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'reply_to' => array(
          'type' => 'text',
          'label' => t('Reply e-mail address'),
          'description' => t("The address to reply to. Leave it empty to use the sender's address."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'list_unsubscribe' => array(
          'type' => 'text',
          'label' => t('Unsubscription e-mail and/or URL'),
          'description' => t("An e-mail address and/or a URL which can be used for unsubscription. Values must be enclosed by angle brackets and separated by a comma."),
          'optional' => TRUE,
        ),
        'attachments' => array(
          'type' => 'text',
          'label' => t('Attachments'),
          'description' => t("The mail's attachments, one file per line e.g. \"files/images/mypic.png\" without quotes."),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t('If specified, the language used for getting the mail message and subject.'),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
      'provides' => array(
        'send_status' => array(
          'type' => 'boolean',
          'label' => t('Send status'),
        ),
      ),
      'base' => 'variable_mimemail_action',
      'access callback' => 'rules_system_integration_access',
    ),
  );
}

/**
 * Action Implementation: Send HTML mail.
 */
function variable_mimemail_action($variable, $to, $cc = NULL, $bcc = NULL, $from_name = NULL, $from_mail = NULL, $reply_to = NULL, $list_unsubscribe = NULL, $attachments = array(), $langcode, $settings, RulesState $state, RulesPlugin $element) {
  module_load_include('inc', 'mimemail');

  $subject_variable_name = str_replace('[mail_part]', 'subject', $variable);
  $body_variable_name = str_replace('[mail_part]', 'body', $variable);
  if (module_exists('i18n_variable')) {
    $subject = i18n_variable_get($subject_variable_name, $langcode, variable_get_value($subject_variable_name));
    $body = i18n_variable_get($body_variable_name, $langcode, variable_get_value($body_variable_name));
  }
  else {
    $subject = variable_get_value($subject_variable_name);
    $body = variable_get_value($body_variable_name);
  }
  // If it's an HTML Mail, make sure we are sending the message content and not an array
  if (is_array($body)) {
    if ($body['format'] == 'php_code') {
      // Execute php
      module_load_include('inc', 'rules', 'modules/php.eval');
      $body = rules_php_eval($body['value'], rules_unwrap_data($state->variables));
    }
    else {
      $body = $body['value'];
    }
  }

  // Set the sender name and from address.
  if (empty($from_mail)) {
    $from = NULL;
  }
  else {
    $from = array(
      'name' => $from_name,
      'mail' => $from_mail,
    );
    // Create an address string.
    $from = mimemail_address($from);
  }

  // Figure out the language to use - fallback is the system default.
  $languages = language_list();
  $language = isset($languages[$langcode]) ? $languages[$langcode] : language_default();

  $params = array(
    'context' => array(
      'subject' => _variable_mimmail_evaluate_token($subject, array(), $state),
      'body' => _variable_mimmail_evaluate_token($body, array(), $state),
      'action' => $element,
      'state' => $state,
    ),
    'cc' => $cc,
    'bcc' => $bcc,
    'reply-to' => $reply_to,
    'list-unsubscribe' => $list_unsubscribe,
    'plaintext' => NULL,
    'attachments' => $attachments,
  );

  // Set a unique key for this mail using variable name
  $key = str_replace('_[mail_part]', '', $variable);

  $message = drupal_mail('mimemail', $key, $to, $language, $params, $from);

  return array('send_status' => !empty($message['result']));
}

/**
 * Copy of _variable_email_evaluate_token() which is not in all releases.
 */
function _variable_mimmail_evaluate_token($text, $options, RulesState $state) {
  module_load_include('inc', 'rules', 'modules/system.eval');
  $var_info = $state->varInfo();
  $options += array('sanitize' => FALSE);

  $replacements = array();
  $data = array();
  // We also support replacing tokens in a list of textual values.
  $whole_text = is_array($text) ? implode('', $text) : $text;
  foreach (token_scan($whole_text) as $var_name => $tokens) {
    $var_name = str_replace('-', '_', $var_name);
    if (isset($var_info[$var_name]) && ($token_type = _rules_system_token_map_type($var_info[$var_name]['type']))) {
      // We have to key $data with the type token uses for the variable.
      $data = rules_unwrap_data(array($token_type => $state->get($var_name)), array($token_type => $var_info[$var_name]));
      $replacements += token_generate($token_type, $tokens, $data, $options);
      // TODO: this code needs to be removed at some point
      // See http://drupal.org/node/1289898
      // Add restricted user tokens (one-time-login-url and cancel-url).
      // For now, it is left commented out
      /*if ($token_type == 'user') {
        user_mail_tokens($replacements, $data, $options);
      }*/
    }
    else {
      $replacements += token_generate($var_name, $tokens, array(), $options);
    }
  }

  // Optionally clean the list of replacement values.
  if (!empty($options['callback']) && function_exists($options['callback'])) {
    $function = $options['callback'];
    $function($replacements, $data, $options);
  }

  // Actually apply the replacements.
  $tokens = array_keys($replacements);
  $values = array_values($replacements);
  if (is_array($text)) {
    foreach ($text as $i => $text_item) {
      $text[$i] = str_replace($tokens, $values, $text_item);
    }
    return $text;
  }
  return str_replace($tokens, $values, $text);
}
